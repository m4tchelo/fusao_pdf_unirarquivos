﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Domain.View
{
    public class ProcessoArquivoView
    {
        public string NomeArquivo { get; set; }
        public MemoryStream ArquivoGerado { get; set; }
    }

    public class DiretorioArquivos 
    {
        public string Diretorio { get; set; }
        public List<string> Arquivos { get; set; }

    }
}
