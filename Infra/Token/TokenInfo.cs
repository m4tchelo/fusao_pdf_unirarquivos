using System;
using System.Collections.Generic;
using System.Reflection;

namespace Infra.Token {
    public class TokenInfo {

        public TokenInfo (object tokeninfo) {
            Type myType = tokeninfo.GetType ();
            IList<PropertyInfo> props = new List<PropertyInfo> (myType.GetProperties ());

            foreach (PropertyInfo prop in props) {
                object propValue = prop.GetValue (tokeninfo, null);
                
                switch (prop.Name) {
                    case "cpf":
                        cpf = propValue.ToString ();
                        break;
                    case "tipoOperador":
                        tipoOperador = propValue.ToString ();
                        break;
                    case "idSessao":
                        idSessao = propValue.ToString ();
                        break;
                    case "clientid":
                        clientid = propValue.ToString ();
                        break;
                }
                // Do something with propValue
            }
        }
        public string cpf { get; set; }
        public string tipoOperador { get; set; }
        public string idSessao { get; set; }
        public string clientid { get; set; }
    }
}