﻿using System;
using System.Collections.Generic;

namespace Infra.Repository._BaseRepository.Interfaces
{
    public interface IRepositoryDapper<T> where T: class
    {
        IEnumerable<T> GetData(string qry, object param = null);
        IEnumerable<T> GetData<TSecond>(string qry, Func<T, TSecond, T> map, string splitOn, object param = null);
        int Execute(string sql, object param = null);
    }
}
