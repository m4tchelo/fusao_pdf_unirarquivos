﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;
using Infra.Dapper;
using Infra.Repository._BaseRepository.Interfaces;

namespace Infra.Repository._BaseRepository
{
    public class RepositoryDapper<T> : IRepositoryDapper<T> where T : class
    {
        private readonly DapperBaseConnection _dapperBaseConnection;

        public RepositoryDapper(DapperBaseConnection dapperBaseConnection)
        {
            _dapperBaseConnection = dapperBaseConnection;
        }

        /// <summary>
        ///  Query using Dapper
        /// </summary>
        /// <param name="qry"></param>
        /// <returns></returns>
        public IEnumerable<T> GetData(string qry, object param = null)
        {
            using (var conexao = new SqlConnection(_dapperBaseConnection.Options.ConnectionString))
            {
                conexao.Open();
                var multi = conexao.QueryMultiple(qry, param, commandTimeout: 60);
                var result = multi.Read<T>();
                return result;
            }
        }

        public IEnumerable<T> GetData<TSecond>(string qry, Func<T, TSecond, T> map, string splitOn, object param = null)
        {
            using (var conexao = new SqlConnection(_dapperBaseConnection.Options.ConnectionString))
            {
                conexao.Open();
                var multi = conexao.Query(qry, map, param, splitOn: splitOn, commandTimeout: 60);
                return multi;
            }
        }

        public int Execute(string sql, object param = null)
        {
            using (var conexao = new SqlConnection(_dapperBaseConnection.Options.ConnectionString))
            {
                conexao.Open();
                var numberOfRowAffected = conexao.Execute(sql, param);
                return numberOfRowAffected;
            }
        }
    }
}
