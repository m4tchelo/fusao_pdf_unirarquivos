using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Extensions.Options;

namespace Infra.Dapper
{
    public partial class DapperBaseConnection
    {
        public readonly DapperOptions Options;

        public IDbConnection Connection;

        public DapperBaseConnection(IOptions<DapperOptions> optionsAccessor)
        {
            Options = optionsAccessor?.Value ?? throw new ArgumentNullException(nameof(optionsAccessor));
            Type type = typeof(SqlConnection);
            Connection = Activator.CreateInstance(type) as IDbConnection;
            Connection.ConnectionString = Options.ConnectionString;
        }
    }
}