﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Infra.Dapper;
using Infra.Repository._BaseRepository;
using Infra.Repository._BaseRepository.Interfaces;
using Infra.Token;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace Infra.UnitOfWork {
    public class UnitOfWork<TContext> : IRepositoryFactory, IUnitOfWork<TContext> where TContext : DbContext, IDisposable {
        private Dictionary<Type, object> _repositories;
        private Dictionary<Type, object> _repositoriesDapper;
        public TContext Context { get; }
        private readonly DapperBaseConnection _dapperBaseConnection;
        private IDbContextTransaction _transaction;
        private IsolationLevel? _isolationLevel;

        public UnitOfWork (TContext context, DapperBaseConnection dapperBaseConnection) {
            Context = context ??
                throw new ArgumentNullException (nameof (context));
            _dapperBaseConnection = dapperBaseConnection;
        }

        public IRepository<TEntity> GetRepository<TEntity> () where TEntity : class {
            if (_repositories == null) _repositories = new Dictionary<Type, object> ();

            var type = typeof (TEntity);
            if (!_repositories.ContainsKey (type)) _repositories[type] = new Repository<TEntity> (Context);
            return (IRepository<TEntity>) _repositories[type];
        }

        public IRepositoryDapper<TEntity> GetRepositoryDapper<TEntity> () where TEntity : class {
            if (_repositoriesDapper == null) _repositoriesDapper = new Dictionary<Type, object> ();
            var type = typeof (TEntity);
            if (!_repositoriesDapper.ContainsKey (type)) _repositoriesDapper[type] = new RepositoryDapper<TEntity> (_dapperBaseConnection);
            return (IRepositoryDapper<TEntity>) _repositoriesDapper[type];
        }

        public IRepositoryAsync<TEntity> GetRepositoryAsync<TEntity> () where TEntity : class {
            if (_repositories == null) _repositories = new Dictionary<Type, object> ();

            var type = typeof (TEntity);
            if (!_repositories.ContainsKey (type)) _repositories[type] = new RepositoryAsync<TEntity> (Context);
            return (IRepositoryAsync<TEntity>) _repositories[type];
        }

        public IRepositoryReadOnly<TEntity> GetReadOnlyRepository<TEntity> () where TEntity : class {
            if (_repositories == null) _repositories = new Dictionary<Type, object> ();

            var type = typeof (TEntity);
            if (!_repositories.ContainsKey (type)) _repositories[type] = new RepositoryReadOnly<TEntity> (Context);
            return (IRepositoryReadOnly<TEntity>) _repositories[type];
        }

        /// <summary>
        /// Defini o nível de isolamento da transação
        /// </summary>
        /// <param name="isolationLevel"></param>
        public void SetIsolationLevel(IsolationLevel isolationLevel)
        {
            _isolationLevel = isolationLevel;
        }
        
        /// <summary>
        /// Força ao UnitOfWork iniciar uma transação
        /// </summary>
        public void ForceBeginTransaction()
        {
            StartNewTransactionIfNeeded();
        }

        /// <summary>
        /// Realizar o commit caso exista uma transação
        /// </summary>
        /// <param name="tokenInfo"></param>
        public void CommitTransaction(Object tokenInfo = null)
        {
            SaveChanges(tokenInfo);

            if (_transaction != null)
            {
                _transaction.Commit();
                _transaction.Dispose();
                _transaction = null;
            }
        }

        /// <summary>
        /// Realiza rollback caso exista uma transação
        /// </summary>
        public void RollbackTransaction()
        {
            if (_transaction != null)
            {
                _transaction.Rollback();
                _transaction.Dispose();
                _transaction = null;
            }
        }
        
        /// <summary>
        /// SaveChanges com dados do Token
        /// </summary>
        /// <param name="tokenInfo"></param>
        /// <returns></returns>
        public int SaveChanges (Object tokenInfo) {
            try {
                if (tokenInfo != null) {
                    var tok = new TokenInfo (tokenInfo);

                    foreach (var entry in Context.ChangeTracker.Entries ().Where (entry => entry.Entity.GetType ().GetProperty ("DataInclusao") != null)) {
                        if (entry.State == EntityState.Added) {
                            entry.Property ("DataInclusao").CurrentValue = DateTime.Now;

                            if (entry.Property ("IdSessao") != null && (int) entry.Property ("IdSessao").CurrentValue == 0)
                                entry.Property ("IdSessao").CurrentValue = Convert.ToInt32 (tok.idSessao);
                        }

                        if (entry.State == EntityState.Modified && entry.Entity.GetType ().GetProperty ("IdSessaoOperacao") != null) {
                            entry.Property ("IdSessaoOperacao").CurrentValue = Convert.ToInt32 (tok.idSessao);
                        }
                    }

                    foreach (var entry in Context.ChangeTracker.Entries ().Where (entry => entry.Entity.GetType ().GetProperty ("IdSessaoOperacao") != null)) {
                        if (entry.State == EntityState.Modified) {
                            entry.Property ("IdSessaoOperacao").CurrentValue = Convert.ToInt32 (tok.idSessao);
                        }
                    }
                }

                return Context.SaveChanges ();
                
            } catch (System.Exception) {

                throw;
            }

        }

        public void Dispose () {
            Context?.Dispose ();
        }
        
        private void StartNewTransactionIfNeeded()
        {
            if (_transaction == null)
            {
                if (_isolationLevel.HasValue)
                {
                    _transaction = Context.Database.BeginTransaction(_isolationLevel.GetValueOrDefault());
                }
                else
                {
                    _transaction = Context.Database.BeginTransaction();
                }
            }
        }
    }
}