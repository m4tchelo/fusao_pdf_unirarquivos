﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using FluentValidation;
using Infra.UnitOfWork;
using Microsoft.EntityFrameworkCore.Query;
using Services._BaseService.Interfaces;

namespace Services._BaseService
{
    public class BaseService<T> : IService<T> where T : class
    {
        private readonly IUnitOfWork _uow;
        public Object _tokenInfo;


        public BaseService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        /// <summary>
        /// Adicionar entidade usando fluent valitador
        /// </summary>
        /// <param name="obj"></param>
        /// <typeparam name="V"></typeparam>
        /// <returns></returns>
        public T Add<V>(T obj) where V : AbstractValidator<T>
        {
            Validate(obj, Activator.CreateInstance<V>());
            var repo = _uow.GetRepository<T>();

            repo.Add(obj);

            //Validate
            _uow.SaveChanges(_tokenInfo);

            return obj;
        }

        /// <summary>
        /// Adicionar entidades
        /// </summary>
        /// <param name="objs"></param>
        /// <returns></returns>
        public virtual IEnumerable<T> AddRange(IList<T> objs)
        {
            var repo = _uow.GetRepository<T>();

            repo.Add(objs);

            _uow.SaveChanges(_tokenInfo);

            return objs;
        }
        
        /// <summary>
        ///  Atualizar Entidade usando Fluent Validator
        /// </summary>
        /// <param name="obj"></param>
        /// <typeparam name="V"></typeparam>
        /// <returns></returns>
        public T Update<V>(T obj) where V : AbstractValidator<T>
        {
            Validate(obj, Activator.CreateInstance<V>());
            var objectUpdate = _uow.GetRepository<T>().Single(x => x == obj);
            if (objectUpdate != null)
                _uow.GetRepository<T>().Update(obj);

            //Validate
            _uow.SaveChanges(_tokenInfo);

            return obj;
        }

        /// <summary>
        /// Deletar Entidade por ID sem validação
        /// </summary>
        /// <param name="id"></param>
        public void Delete(int id)
        {
            if (id == 0)
                throw new ArgumentException("O Id não pode ser zero.");

            var repo = _uow.GetRepository<T>();
            repo.Delete(id);

            //Validate
            _uow.SaveChanges(_tokenInfo);
        }


        /// <summary>
        /// Deletar Entidade por ID sem validação
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        public void Delete(T entity)
        {
            if (entity == null)
                throw new ArgumentException("Entidade não encontrada");

            var repo = _uow.GetRepository<T>();
            repo.Delete(entity);

            //Validate
            _uow.SaveChanges(_tokenInfo);
        }

        /// <summary>
        /// Recuperar todas as informações da entidade
        /// </summary>
        /// <returns></returns>
        public IEnumerable<T> GetAll(
            Expression<Func<T, bool>> predicate = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null,
            int index = 0,
            int size = 0, 
            bool disableTracking = true
        )
        {
            //Set repository you will working
            var repo = _uow.GetRepository<T>();
            return repo.GetList(predicate, orderBy, include, index, size, disableTracking).Items.AsEnumerable();
        }

        public T GetById(Expression<Func<T, bool>> predicate, Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null)
        {
            //Set repository you will working
            var repo = _uow.GetRepository<T>();
            //return 
            return repo.Select(predicate, include);
        }

        public IEnumerable<T> Query(string sql, object param = null)
        {
            //Set repository you will working
            //var repo = _uow.GetRepository<T>();
            var repo = _uow.GetRepositoryDapper<T>();
            //return
            //return repo.Query(sql).AsEnumerable();
            return repo.GetData(sql).AsEnumerable();
        }

        private void Validate(T obj, AbstractValidator<T> validator)
        {
            if (obj == null)
                throw new Exception("Registros não detectados!");

            validator.ValidateAndThrow(obj);
        }
    }
}
