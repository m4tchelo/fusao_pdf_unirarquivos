using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using Domain.View;
using Infra.UnitOfWork;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.SqlServer.Server;
using Serilog;
using Services._BaseService;
using Services._BaseService.Interfaces;

namespace Services
{
    public class ProcessarService
    {
        private readonly IHostEnvironment _hostEnvironment;
        private readonly IConfiguration _configuration;

        public ProcessarService(
            IHostEnvironment hostEnvironment,
            IConfiguration configuration
            )
        {
            _hostEnvironment = hostEnvironment;
            _configuration = configuration;
        }

        public void ProcessarArquivo(List<string> SolicitacaoProcesso)
        {
            Log.Information($"Iniciado processamento arquivos/diretorios {SolicitacaoProcesso.Count()} diretorio(s)");
            string ArquivoExiste = "";
            bool concluido = false;
            bool Error = false;
            int OrdemApenso = 1;
            List<ProcessoArquivoView> ArquivosProcesso = new List<ProcessoArquivoView>();
            List<DiretorioArquivos> DiretorioArquivos = new List<DiretorioArquivos>();
            try
            {
                //==================================================================================================================================
                //CONSULTA arquivos de diretorios 
                var start = DateTime.Now;

                //Busca arquivos de cada pasta apenas .pdf .PDF
                foreach (var item in SolicitacaoProcesso)
                {
                    if (Directory.Exists(item))
                    {
                        var arquivos = Directory.GetFiles(item).ToList();

                        DiretorioArquivos.Add(new DiretorioArquivos
                        {
                            Diretorio = item,
                            Arquivos = arquivos
                        });
                    }
                    else 
                    {
                        Log.Debug($"Diretorio {item}, não Existe.");
                    }
                }

                //CHAMA FUNÇÃO PARA CRIAR OS ARUIVOS EM MEMORIA.
                if (DiretorioArquivos.Count() > 0)
                {
                    //uni arquivos pdf contidos dentro de cada pasta
                    foreach (var ProcessaPasta in DiretorioArquivos)
                    {
                        var processoApensadoRetorno = CriaProcessoMemoria(ProcessaPasta, $"Ordem_{OrdemApenso}_");
                        if (processoApensadoRetorno != null)
                            ArquivosProcesso.Add(processoApensadoRetorno);

                        OrdemApenso += 1;
                    }


                    //APENAS CRIA ARQUIVO ZIP QUANDO A QUANTIDADE DE ARQUIVOS GERADOS É IGUAL A QUANTIDADE DE SOLICITAÇÃO   "SolicitacaoProcesso"
                    var ProcessosConsultados = SolicitacaoProcesso.Count();
                    if (ArquivosProcesso.Count() == ProcessosConsultados)
                    {
                        Log.Information($"Criando arquivo zip de pdf(s) dos diretorios solicitados");

                        var nomeArquivoZip = SalvaArquivoZip(ArquivosProcesso, _configuration["Pastas:ProcessoTemp"],$"Arquivos_Agrupados_{DateTime.Now.ToString("yyyyddMM_")}");

                        ArquivoExiste = _configuration["Pastas:ProcessoTemp"] + nomeArquivoZip;
                        Log.Information($"Arquivo {nomeArquivoZip} gerado em {ArquivoExiste}");

                        concluido = true;
                    }


                }
                else
                {
                    Log.Information($"lISTA DE DIRETORIOS VAZIA");

                }

                var fim = (DateTime.Now);

                Log.Information("execução concluída com início em " + start + " e fim em " + fim + " = " + (fim - start).TotalSeconds + " segundos");

            }
            catch (Exception e)
            {
                Error = true;

                //REGISTRAR LOG
                Log.Error(e, $"Erro no processamento");

            }
            finally
            {
                //AO FINALIZAR, comunicar com o RABBIT MQ
                if (concluido && File.Exists(ArquivoExiste))
                {
                    Log.Information("PROCESSAMENTO FINALIZADO COM SUCESSO");
                }
                else if (!Error)
                {
                    Log.Information("PROCESSAMENTO FINALIZADO COM ERROS");
                }
            }
        }
        public string SalvaArquivoZip(List<ProcessoArquivoView> arquivoSPDF, string diretorio, string nomeArquivoPDF)
        {
            var nomeArquivoZip = nomeArquivoPDF.ToUpper().Contains(".PDF") ? nomeArquivoPDF.Replace(".pdf", ".zip") : nomeArquivoPDF+".zip";
            using (var zipstream = new MemoryStream())
            {
                using (var archive = new ZipArchive(zipstream, ZipArchiveMode.Create, true))
                {
                    //lista de arquivos PDF memoria
                    foreach (var arquivoPDF in arquivoSPDF)
                    {
                        byte[] fileBytes = arquivoPDF.ArquivoGerado.ToArray();
                        var fileInArchive = archive.CreateEntry(arquivoPDF.NomeArquivo, CompressionLevel.Optimal);
                        using (var entryStream = fileInArchive.Open())
                        using (var fileToCompressStream = new MemoryStream(fileBytes))
                        {
                            fileToCompressStream.CopyTo(entryStream);
                        }
                    }

                }
                //Grava arquivo Zip
                using (FileStream file = new FileStream(diretorio + "\\" + nomeArquivoZip, FileMode.Create, System.IO.FileAccess.Write))
                {
                    var salvazip = new MemoryStream();
                    byte[] byteInfo0 = zipstream.ToArray();
                    salvazip.Write(byteInfo0, 0, byteInfo0.Length);
                    salvazip.Position = 0;
                    salvazip.CopyTo(file);
                }
            }

            return nomeArquivoZip;
        }
        public void AcessoDiretorio()
        {
            var mensagem = "";
            var dirTemp = _configuration["Pastas:ProcessoTemp"];
            mensagem = "";
            try
            {
                var dirAcesso = new DirectoryInfo(dirTemp).Attributes;
            }
            catch (Exception e)
            {
                mensagem += e;
                Log.Information($"Aplicação sem acesso ao diretorio {dirTemp}: {mensagem}");

            }
        }

        public ProcessoArquivoView CriaProcessoMemoria(DiretorioArquivos processo, string Ordem)
        {
            int qtdInformacaoAdicionado = 0;
            int qdtInformacaoProcesso = 0;

            //DIRETORIO ORIGEM ARQUIVOS SE PRECISAR  
            var DirInfoPDF = _configuration["Pastas:PastaInformacoesPDF"];

            //Log de acesso a diretorio, caso não tenha acesso
            AcessoDiretorio();

            //CONTA QUANTIDADE DE ARQUIVOS A SEREM PROCESSADOS
            qdtInformacaoProcesso = processo.Arquivos.Count();


            //TENTA GERAR ARQUIVO QUANDO A QUANTIDADE DE INFORMAÇÃO FOR MAIOR QUE "0"
            if (qdtInformacaoProcesso > 0)
            {
                MemoryStream compressedBytes = new MemoryStream();
                using (var ArquivoProcesso = new MemoryStream())
                {
                    var documento = new Document();
                    PdfReader reader = null;
                    try
                    {
                        var copy = new PdfCopy(documento, ArquivoProcesso);
                        documento.Open();
                        foreach (var informacao in processo.Arquivos)
                        {
                            if (informacao != null)
                            {
                                ////concatena os arquivos PDF
                                //var DIRETORIO = DirInfoPDF + informacao.Informacao.Setor.Trim() + "\\";
                                //var ARQUIVONAME = $"{informacao.Informacao.Setor.Trim()}_{informacao.Informacao.NumeroProcesso}_{informacao.Informacao.AnoProcesso}_{informacao.Informacao.Ordem.ToString().PadLeft(4, '0')}.pdf";

                                if (File.Exists(informacao))
                                {
                                    qtdInformacaoAdicionado++;
                                    reader = new PdfReader(informacao);
                                    var paginas = reader.NumberOfPages;
                                    //Add pages of current file
                                    for (int i = 1; i <= paginas; i++)
                                    {
                                        var importedPage = copy.GetImportedPage(reader, i);
                                        copy.AddPage(importedPage);
                                    }
                                    reader.Close();
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        reader?.Close();
                        throw new Exception(e.ToString());
                    }

                    byte[] byteInfo = ArquivoProcesso.ToArray();
                    compressedBytes.Write(byteInfo, 0, byteInfo.Length);
                    compressedBytes.Position = 0;

                    //nome do arquivo unido 
                    var nomeArquivo = $"{Ordem}{DateTime.Now.ToString("yyyyMMdd-HHmmssffff")}.pdf";

                    //considerado arquivo criado quando a quantidade de arquivos é igual a quantidade de informação adicionada na união dos arquivos PDF
                    if (qdtInformacaoProcesso == qtdInformacaoAdicionado)
                    {
                        Log.Information($"Criando arquivo de pdf Diretorio {processo.Diretorio}");
                        documento.Close();
                        var ArquivoRetorno = new ProcessoArquivoView
                        {
                            NomeArquivo = $"{nomeArquivo}",
                            ArquivoGerado = ArquivoProcesso,
                        };
                        return ArquivoRetorno;
                    }
                }
            }
            return null;
        }
    }
}