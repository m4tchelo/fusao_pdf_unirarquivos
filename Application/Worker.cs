using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Services;

namespace Application
{
    public class Worker : BackgroundService
    {
        private readonly IConfiguration _configuration;
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public Worker(IConfiguration configuration, IServiceScopeFactory serviceScopeFactory)
        {
            _configuration = configuration;
            _serviceScopeFactory = serviceScopeFactory;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                var intervaloVerificacao = int.Parse(_configuration["ApplicationInfo:IntervaloVerificacao"]);

                //while (!stoppingToken.IsCancellationRequested)
                //{
                    try
                    {
                        Log.Debug("Verificando solicita�oes pendentes de processamento...");
                        //AQUI SERIA SE A APLICA��O CONSULTASSE O BANCO PARA CRIAR ARQUIVO DE PROCESSO 
                        List<string> SolicitacaoProcesso = new List<string>();
                        //Essa informa��o esta estatica mas poderia ser realizado consulta em banco para o processamento
                        //Lista de arquivos a serem unidos, dai seria conforme a aplicabilicadade de uso da aplica��o.
                        //pos o importante � o processamento  "processarService.ProcessarArquivo(SolicitacaoProcesso);"
                        var diretorioAplicacao = System.IO.Directory.GetCurrentDirectory();
                        SolicitacaoProcesso.Add($@"{diretorioAplicacao}\Content\Dir_A");
                        SolicitacaoProcesso.Add($@"{diretorioAplicacao}\Content\Dir_B");
                        SolicitacaoProcesso.Add($@"{diretorioAplicacao}\Content\Dir_C");
                        SolicitacaoProcesso.Add($@"{diretorioAplicacao}\Content\Dir_D");

                        if (SolicitacaoProcesso.Count > 0) 
                        Log.Debug($"Quantidade de arquivos para serem processados: {SolicitacaoProcesso.Count()}");


                        using (var scope = _serviceScopeFactory.CreateScope())
                        {
                            var processarService = scope.ServiceProvider.GetRequiredService<ProcessarService>();
                            processarService.ProcessarArquivo(SolicitacaoProcesso);
                        }


                        await Task.Delay(intervaloVerificacao, stoppingToken);

                    }
                    catch (Exception e)
                    {
                        Log.Error(e, "Erro inesperado.");
                    }
                //}
            }
            catch (Exception e)
            {
                Log.Error(e, "Erro inesperado.");
            }
        }
    }
}