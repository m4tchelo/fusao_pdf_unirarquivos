﻿using AutoMapper;
using FluentValidation;
using Infra.Contexts;
using Infra.Dapper;
using Infra.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Services;
using Services._BaseService;
using Services._BaseService.Interfaces;
using System.IO;

namespace Application
{
    public class Startup
    {
        public static void ConfigureServices(HostBuilderContext hostContext, IServiceCollection services)
        {


            var configuration = new ConfigurationBuilder()
                    .SetBasePath(hostContext.HostingEnvironment.ContentRootPath)
                    .AddJsonFile($"connectStrings.json", false, true)
                    .AddJsonFile("appsettings.json", false, true)
                    .AddEnvironmentVariables()
                    .Build();
            
            services.AddHostedService<Worker>();

            services.AddAutoMapper(typeof(Startup));
            services.AddSingleton<IConfiguration>(configuration);

            // Services
            //services.AddTransient<IUnitOfWork, UnitOfWork<ProcessoContext>>();
            services.AddTransient<ProcessarService>();

        }
    }
}