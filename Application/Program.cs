using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Diagnostics;

namespace Application
{
    public class Program
    {
        public static void Main(string[] args)
        {
            

            try
            {
                //Diretorio aplicação build bin
                var diretorio1 = AppDomain.CurrentDomain.BaseDirectory;
                //diretorio1 aplicação Aplication
                var diretorio2 = System.IO.Directory.GetCurrentDirectory();
                var configuration = new ConfigurationBuilder()
                    .AddJsonFile("appsettings.json")
                    .Build();

                Log.Logger = new LoggerConfiguration()
                    .ReadFrom.Configuration(configuration)
                    .WriteTo.File($"{diretorio2}/logs/log-.json", rollingInterval: RollingInterval.Day)
                    .CreateLogger();



                //AppLoggerExtensionsServico.ConfigureSerilogLoggers();
                Log.Information($"Iniciando aplicação. Ambiente: {diretorio1}");
                Log.Information($"Iniciando aplicação. Ambiente: {diretorio2}");
                Log.Information($"Iniciando aplicação. Ambiente: {configuration["ApplicationInfo:Environment"]}");
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Falha ao iniciar aplicação");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSerilog()
                .UseWindowsService()
                .ConfigureServices((hostContext, services) => { Startup.ConfigureServices(hostContext, services); });
    }
}